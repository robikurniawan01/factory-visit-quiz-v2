# Factory Visit Quiz

## Project Information

**Factory Visit Quiz** adalah aplikasi yang dirancang untuk meningkatkan pengalaman pengunjung selama kunjungan ke pabrik dengan menyediakan kuis interaktif. Kuis ini mencakup berbagai pertanyaan terkait operasi pabrik, protokol keselamatan, dan pengetahuan umum tentang perusahaan. Pengunjung dapat mengikuti kuis ini, dan hasilnya akan segera dihitung serta diberikan umpan balik secara langsung.

Aplikasi ini juga memungkinkan administrator untuk membuat dan mengelola pertanyaan kuis, serta menghasilkan laporan tentang partisipasi dan kinerja kuis.

## Features

- **Interactive Quizzes**: Pengunjung dapat menjawab pertanyaan terkait pabrik dan mendapatkan umpan balik langsung.
- **Question Management**: Administrator dapat menambah, mengubah, dan menghapus pertanyaan kuis.
- **Performance Reports**: Laporan tentang partisipasi dan kinerja kuis dapat dihasilkan untuk analisis lebih lanjut.

## Technologies Used

- **Frontend**: 
  - HTML
  - CSS
  - JavaScript

- **Backend**: 
  - Node.js
  - Express.js

- **Database**: 
  - MySQL

## Installation

1. **Clone the repository**
   ```bash
   git clone https://github.com/yourusername/factory-visit-quiz.git
   cd factory-visit-quiz
